<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('servers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('logged_user', 191)->comment('Usuario que inició sesión.');
            $table->string('os_name', 191)->comment('Nombre y versión del sistema operativo.');
            $table->string('cpu', 191)->comment('Nombre de la CPU.');
            $table->string('ip', 191)->comment('Ip del servidor.');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('servers');
    }
}
