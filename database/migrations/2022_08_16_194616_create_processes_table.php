<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProcessesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('processes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('pid', 191)->comment('PID del proceso');
            $table->string('username', 191)->nullable()->comment('Usuario del proceso');
            $table->longText('name', 191)->comment('Nombre del proceso');
            $table->string('vms', 191)->comment('Memoria usada del proceso');
            $table->unsignedBigInteger('server_id')->comment('Identificador del servidor');
            $table->timestamps();
            $table->foreign('server_id')->references('id')->on('servers')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('processes');
    }
}
