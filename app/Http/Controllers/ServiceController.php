<?php

namespace App\Http\Controllers;

use App\Exports\ProcessExport;
use App\Exports\ServerExport;
use App\Http\Requests\ServerExcelRequest;
use App\Http\Requests\ServerRequest;
use App\Http\Resources\ServerResource;
use App\Server;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ServiceController extends Controller
{
    public function index(Request $request)
    {
        $data = Server::with('processes')->paginate(
            $request->get('per_page', 15)
        );
        return ServerResource::collection($data)->additional([
            'general_report_to_excel' => route('service.export')
        ]);
    }

    public function excel(ServerExcelRequest $request)
    {
        try {
            if ($request->has('server_id')) {
                $server = Server::findOrFail($request->get('server_id'));
                $now = now()->format('Y-m-d-H:i:s');
                return (new ServerExport)
                    ->forServer($request->get('server_id'))
                    ->download("{$server->ip}_{$now}.xlsx");
            }
            return (new ProcessExport)->download('REPORTE_GENERAL.xlsx');
        } catch (\Exception $exception) {
            return response()->json(
                $exception->getMessage(),
                400
            );
        }
    }

    public function store(ServerRequest $request)
    {
        try {
            DB::beginTransaction();
            $server = new Server();
            $server->fill($request->all());
            $server->save();
            $server->processes()->createMany(
                $request->get('process')
            );
            DB::commit();
            return response()->json('Procesos almacenados satisfactoriamente');
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(
                $e->getMessage(),
                400
            );
        }
    }
}
