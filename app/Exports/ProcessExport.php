<?php

namespace App\Exports;

use App\Process;
use App\Server;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithTitle;

class ProcessExport implements FromQuery, WithHeadings, WithMapping, WithTitle, ShouldAutoSize
{
    use Exportable;

    public function query()
    {
        return Process::query();
    }

    public function headings(): array
    {
        return [
            'ID SERVIDOR',
            'USUARIO SERVIDOR',
            'SISTEMA OPERATIVO',
            'CPU',
            'IP',
            'PID PROCESO',
            'USUARIO PROCESO',
            'NOMBRE PROCESO',
            'VMS PROCESO',
        ];
    }

    public function map($row): array
    {
        return [
            'server_id'            => isset($row->server->id) ? (int) $row->server->id : '',
            'logged_user'          => $row->server->logged_user ?? '',
            'os_name'              => $row->server->os_name ?? '',
            'cpu'                  => $row->server->cpu ?? '',
            'ip'                   => $row->server->ip ?? '',
            'pid'                  => $row->pid ?? '',
            'username'             => $row->username ?? '',
            'name'                 => $row->name ?? '',
            'vms'                  => $row->vms ?? '',
        ];
    }

    public function title(): string
    {
        return 'SERVICIOS';
    }
}
