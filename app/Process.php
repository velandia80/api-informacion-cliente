<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Process extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'processes';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'pid',
        'username',
        'name',
        'vms',
        'server_id'
    ];

    public function server()
    {
        return $this->belongsTo(Server::class);
    }
}
